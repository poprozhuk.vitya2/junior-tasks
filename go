package main

import (
	"fmt"
	"github.com/narqo/go-badge"
	"html/template"
	"log"
	"net/http"
)

func badgeSvg(subject string, status string, colorSVG badge.Color) string {
	svg, err := badge.RenderBytes(
		subject,
		status,
		colorSVG,
	)
	if err != nil {
		panic(err)
	}
	return string(svg)
}

func svg1(w http.ResponseWriter, r *http.Request) {
	image := badgeSvg("godoc", "reference", "#e05d44")

	t, err := template.New("webpage").Parse(text(image))
	if err != nil {
		log.Fatal(err)
	}
	t.Execute(w, nil)
}

func svg2(w http.ResponseWriter, r *http.Request) {
	image := badgeSvg("godoc", "reference", "#9f9f9f")

	t, err := template.New("webpage").Parse(text(image))
	if err != nil {
		log.Fatal(err)
	}
	t.Execute(w, nil)
}

func text(svg string) string {
	return fmt.Sprintf(`	<h1> Log </h1> <img>%s</img>`, svg)
}

func main() {

	http.HandleFunc("/v1/1/counter.svg", svg1)
	http.HandleFunc("/v1/2/counter.svg", svg2)

	http.ListenAndServe(":8080", nil)
}
